if [ $# -ne 2 ]
then
	echo "USAGE: create_graph [-g|-t] <mojo model>"
	exit 1
fi

if [ "$1" = "-g" ]
then  
	java -cp h2o.jar hex.genmodel.tools.PrintMojo --tree 0 -i $2 -o tmp_model.gv -f 20 -d 3
	dot -Tpng tmp_model.gv -o $2_g.png
	rm tmp_model.gv	
else
	java -cp h2o-genmodel.jar hex.genmodel.tools.PrintMojo --tree 0 -i $2 -o $2_t.png --format png
fi
