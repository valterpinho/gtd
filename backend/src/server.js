const Koa = require('koa');
const Router = require('@koa/router');
const { runCodeStandardization } = require('../../gtdtools/ml-tools');
const app = new Koa();
const router = new Router();
const koaBody = require('koa-body');
const bodyParser = require('koa-bodyparser');

const GTD = require ('../../gtdtools/gtdclasses')

// Env variables
const {
    GTD_MDW_HOST,
    GTD_MDW_PORT,
    GTD_WEBAPP_HOST,
    GTD_WEBAPP_PORT
} = process.env;

router.get('status', '/', (ctx) => {
    ctx.body = 'Hello World';
});

router.post('vies', '/vies', async (ctx) => {
    console.log(ctx.request.body);
    const { sourceItemCode, description, country, metadata, event } = ctx.request.body;
    console.log("BODY CONTENT VIES:", sourceItemCode, description, country, metadata, event );

    // check VIES 

    const C = new GTD(ctx.request.body);
    const viesRet = await C.checkVies();

    console.log( "VIES=", viesRet);

    ctx.body = { response: "VIES route" };
    ctx.status = 200
});

router.post('standardize', '/standardize', (ctx) => {
    console.log(ctx.request.body);
    const { sourceItemCode, description, countryCode, metadata, event } = ctx.request.body;
    console.log("BODY CONTENT STANDARD:", sourceItemCode, description, countryCode, metadata, event );
    console.log("BODY CONTENT", sourceItemCode, description);
    ctx.body = { response: "STANDARDIZE route" };
    ctx.status = 200
});

router.post('determination', '/determine', (ctx) => {
    console.log(ctx.request.body);
    const { sourceItemCode, description, countryCode, metadata, event  } = ctx.request.body;
    console.log("BODY CONTENT DETERMINATION:", sourceItemCode, description, countryCode, metadata, event );
    ctx.body = { response: "TAX DETERMINATION route" };
    ctx.status = 200
});

app
    .use(koaBody())
    .use(bodyParser())
    .use(router.routes())
    .use(router.allowedMethods());

app.listen(GTD_MDW_PORT);