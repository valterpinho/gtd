const CS = require('./standardizationMethods');
const GP = require('./genPurposeTools.js')
const GTDC = require('./gtdclasses.js');

const listItems = require ( "./listItems.json");
console.log (listItems);

listItems.itemsToCheck.forEach ( e => {

  let classItem= new GTDC(e);

  let itemSKU = classItem.getSKU();
  let supTRN = classItem.getSupplierTRN();

  console.log (supTRN);
  console.log ( itemSKU);

  CS.runCodeStandardization(e).then ( 
    (result) => {

      let jsonRes = JSON.parse(result);

      console.log("Classification on ", e);

      console.log("HS6 Code\tWeight\tDescription");
      jsonRes.classifications.forEach(element => {
        console.log(element.hs6code, "\t\t", element.vectorWeight, "\t", element.hurricaneDescription)
      });

      console.log ( "Flatten:", GP.flattenObject ( e));

    })

})
