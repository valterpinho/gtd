const ML = require("./ml-tools.js");

const PurchaseLinesNrOfColumns = 9;
const PurchaseLinesColumnNames =
  `"SupplierTRN",\
    "SKU",\
    "ProductDescription",\
    "TaxDate",\
    "TaxExemptionCode",\
    "TaxType",\
    "TaxCountry",\
    "TaxCode",\
    "TaxPercentage" \
    `;

const PurchaseLinesDataTypes =
  `"Enum",\
    "Enum",\
    "String",\
    "Time",\
    "Enum",\
    "Enum",\
    "Enum",\
    "Enum",\
    "Enum"\
    `;

const FILE = "/Users/valterpinho/dev/data-samples/PurchaseLines_all.csv";
const SOURCE_FRAME = "nfs://Users/valterpinho/dev/data-samples/PurchaseLines_all.csv";
const DESTINATION_FRAME = "PurchaseInvoiceLines";
const MODEL_NAME = "GBM_ModelOnPurchases";
const DOWNLOAD_PATH = "/Users/valterpinho/dev/gtd/mojo"

ML.importSourceData(FILE)
  .then((ret) => {

    console.log("\n#INFO Finished Importing file\n\n");
    ML.parseFrame(
      SOURCE_FRAME,
      DESTINATION_FRAME,
      PurchaseLinesNrOfColumns,
      PurchaseLinesColumnNames,
      PurchaseLinesDataTypes
    )
      .then(() => {

        ML.checkFrame(DESTINATION_FRAME, 3)
          .then((ret) => {

            console.log("FRAME CHECKED");

            ML.splitFrames(DESTINATION_FRAME, 0.75, 761253)
              .then((ret) => {
                console.log("\n#INFO Finished Splitting \n\n");
                console.log("\n#INFO Starting GBM Model \n\n");

                ML.checkFrame(DESTINATION_FRAME, 3).then(() => {
                  ML.buildGBM(MODEL_NAME, `${DESTINATION_FRAME}_Train`, `${DESTINATION_FRAME}_Test`, "TaxCode")
                    .then((res) => {
                      console.log(">>", res, "\n\n");
                      ML.checkJob(res, 20)
                        .then(
                          (res) => {
                            console.log("Job Status:", res);
                            ML.saveMojo(res, MODEL_NAME, `${DOWNLOAD_PATH}/${MODEL_NAME}.zip`)
                          }
                        )
                    })
                })
              })
          })
          .catch((err) => { console.log("Catched: Error on split!", err) })
      })
      .catch((err) => { console.log("Catched: Error in parse!", err) })

  })
  .catch((err) => { console.log("Catched: Error in frame!", err) })
