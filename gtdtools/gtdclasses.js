const ML = require("./ml-tools.js");
const VIES = require("./vies.js");
const GP = require("./genPurposeTools.js")

class GTDData {
    constructor(ctx) {
        this.SKU = ctx["sku"];
        this.Description = ctx["description"];
        this.Country = ctx["country"];
        this.Language = ctx["language"];
        this.Metadata = ctx["metadata"]
        this.Event = ctx["event"];
    }

    // Getters

    getSKU() {
        return GP.purgeReturn(this.SKU);
    }

    getDescription() {
        return GP.purgeReturn(this.Description);
    }

    getCountry() {
        return GP.purgeReturn(this.Country);
    }

    getLanguage() {
        return GP.purgeReturn(this.Language);
    }

    getMetadata() {
        return GP.purgeReturn(this.Metadata);
    }

    getEvent() {
        return GP.purgeReturn(this.Metadata);
    }

    

    /**
     * getSupplierTRN method 
     * @returns Object with Supplier Tax Reg Number and country
     */
    getSupplierTRN() {
        console.log("GPpurge", GP.purgeReturn(this.Metadata));
        if (GP.purgeReturn(this.Metadata) == "empty")
            return ({ "supplierTRN": "empty", "country": "empty" })

        console.log("SUPTRN:", this.Metadata.supplierTRN);
        if (this.Metadata.supplierTRN == undefined)
            return ({ "supplierTRN": "empty", "country": "empty" })
        else {
            let supTRNi = this.Metadata;

            console.log("CLASS supplierTRN", typeof (supTRNi));
            console.log("supplierTRN", supTRNi.supplierTRN);

            return ({
                "supplierTRN": supTRNi.supplierTRN,
                "country": this.Country
            }
            );
        }
    }

    /**
     * getSupplierTRN method 
     * @returns Object with Supplier Tax Reg Number and country
     */
    getTaxVAT() {
        console.log("GPpurge", GP.purgeReturn(this.Metadata));
        if (GP.purgeReturn(this.Event) == "empty")
            return ({ "originalTax": "empty" })

        console.log("SUPTRN:", this.Metadata.supplierTRN);
        if (this.Event.tax_vat == undefined)
            return ({ "originalTax": "empty" })
        else {
            return ({ "originalTax": this.Event.tax_vat })
        }
    }

    getAllFlatten() {
        return (GP.flattenObject(this));
    }

    /** end getters **/

    // Method for code standardization
    /**
     * 
     * @returns {object} List of suggested standardizations 
     */
    async codeStandardization() {
        const returnStandard = await ML.runCodeStandardization({ sourceCode: this.Description });

        return ({
            sourceCode: this.Description,
            standardCode: returnStandard
        })
    }

    // Method for checking VIES on tax reg number
    /**
     * 
     * @returns {object} List of suggested standardizations 
     */
    async checkVies() {

        let supTRN = this.getSupplierTRN();
        let countryCode = this.getCountry();

        if (supTRN != "NONE") {
            const retCheck = await VIES.checkVies(supTRN, countryCode);
            console.log("retcheck:", retCheck);
            return (retCheck);
        }
        else
            return ("No VIES");
    }

    /**
     * taxDetermination 
     * @returns {object} Suggested tax determination 
     */
    async taxDetermination() {
        const returnStandard = await ML.runTaxDetermination({ sourceCode: this.Description });

        return {
            'event.tax_vat': 0,
            'taxVATsuggestion': returnStandard
        }
    }

}

module.exports = GTDData 