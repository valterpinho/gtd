/**
 * Machine Learning Tools
 */

var request = require('request');

const hurricaneURL = 'https://api.hrrcn.io/v3.2/classification';
const apiKey = '8SEUhIAZqg6iWwyTsiGX48tjSLCkPQz0aD9BuqEU';

const H2O_HOST = "http://127.0.0.1:54321"
const H2O_API_PREFIX = "/3/"

// http://127.0.0.1:54321/3/


/**
 * hurricaneClassification : Call Hurricane Commerce API for classification 
 * @param {*} sku 
 * @param {*} description 
 * @param {*} country 
 * @param {*} language 
 * @returns object with 
 */
async function hurricaneClassification(sku, description, country, language) {
  return new Promise((res, rej) => {

    let requestBody = JSON.stringify(
      {
        "clintSKU": sku,
        "clientDescription": description,
        "sourceCountry": country,
        "sourceLanguage": language,
      }
    );

    const options = {
      'method': 'POST',
      'url': hurricaneURL,
      'headers': {
        'Content-Type': 'application/json',
        'X-Api-Key': apiKey
      },
      body: requestBody

    };

    request(options, (error, response) => {
      if (error) {
        rej(error);
        return (error);
      }

      if (response.statusCode == 404) {
        res("Error");
      }

      res(response.body);

    })

  })
}

async function runTaxDetermination(param) {
  return (runCodeStandardization(param));
}

/**
 * Import raw data into h2o.ai 
 * @param {string} path to file to import
 * @returns 
 */
async function importSourceDataH2O(file) {
  let Route = "ImportFiles?path="
  return new Promise((res, rej) => {
    var options = {
      'method': 'GET',
      // 'http://127.0.0.1:54321/3/ImportFiles?path=/Users/valterpinho/Downloads/${frame}.csv'
      'url': `${H2O_HOST}${H2O_API_PREFIX}${Route}${file}`,
      'headers': {
      }
    };

    request(options, (error, response) => {
      if (error) {
        rej(error);
        return (error);
      }

      if (response.statusCode == 404) {
        res("Error");
      }

      res(response.body);

    })

  })
}

/**
 * Return frame information by ID
 * @param {String} frameID 
 * @returns 
 */
async function getFrame(frameID) {
  let Route = "Frames";
  return new Promise((res, rej) => {
    var options = {
      'method': 'GET',
      // 'http://127.0.0.1:54321/3/ImportFiles?path=/Users/valterpinho/Downloads/${frame}.csv'
      'url': `${H2O_HOST}${H2O_API_PREFIX}${Route}/${frameID}`,
    };

    request(options, (error, response) => {
      if (error) {
        rej(error);
        return (error);
      }
      if (response.statusCode == 404) { res("Not found"); }
      res(response.body);
    });
  })
}

/**
 * Check if a frame with (frameID) exists a number of times/each 2 seconds
 * @param {String frameID 
 * @param {Int} numRetries 
 * @returns 
 */
function checkEveryNSeconds(frameID, seconds) {
  return new Promise((res) => {
    setTimeout(() => {
      res(getFrame(frameID))
    }, seconds * 1000);
  }
  )
}
async function checkFrame(frameID, numRetries) {

  var r = await getFrame(frameID);
  var retry = 1;

  while ((retry < numRetries)) {
    r = await checkEveryNSeconds(frameID, 1);
    console.log(retry);
    retry++;
  }
  if (r === 'Not found') {
    throw new Error("Frame not Found");
  }

  return (r);
}




/**
 * getAllFrames() 
 * @returns All frames on h2o
 */
async function getAllFrames() {
  let Route = "Frames";
  return new Promise((res, rej) => {
    var options = {
      'method': 'GET',
      // 'http://127.0.0.1:54321/3/ImportFiles?path=/Users/valterpinho/Downloads/${frame}.csv'
      'url': `${H2O_HOST}${H2O_API_PREFIX}${Route}`,
      'headers': {
      }
    };

    request(options, (error, response) => {
      if (error) {
        rej(error);
        return (error);
      }
      if (response.statusCode == 404) { res("Not found"); }
      res(response.body);
    });

  })
}



/**
 * Return job status information by jobID
 * @param {string} jobID 
 * @returns job status or "Not found" 
 */
async function getJobStatus(jobID) {
  let Route = "Jobs";
  return new Promise((res, rej) => {
    var options = {
      'method': 'GET',
      'url': `${H2O_HOST}${H2O_API_PREFIX}${Route}`,
    };

    request(options, (error, response) => {
      if (error) {
        rej(error);
        return (error);
      }
      if (response.statusCode == 404) { res("Not found"); }

      /* find jobID */
      for (const element of JSON.parse(response.body).jobs) {
        l = JSON.stringify(element);
        if (element.key.name == jobID) {
          res(element.status);
        }
      }

      res("Not found");
    });
  })
}



/**
 * Check if a job is done with a stop of (numRetries)
 * @param {*} jobID 
 * @param {*} numRetries 
 * @returns 
 */

function checkAgain(jobID, seconds) {
  return new Promise((res) => {
    setTimeout(() => {
      res(getJobStatus(jobID))
    }, seconds * 1000);
  }
  )
}

async function checkJob(jobID, numRetries) {

  var r = await getJobStatus(jobID);
  var retry = 1;

  while ((retry < numRetries)) {
    r = await checkAgain(jobID, 1);

    if ( r == "DONE") {
      return (r);
    }

    console.log(retry);
    retry++;
  }
  if (r === 'Not found') {
    throw new Error("Job not Found");
  }

  return (r);
}

/**
 * Parse the imported files into a frame
 * @param {*} sourceFrame 
 * @param {*} destinationFrame 
 * @param {*} nrOfColumns 
 * @param {*} columnNames 
 * @param {*} columnTypes 
 * @returns 
 */
async function parseFrame(sourceFrame, destinationFrame,
  nrOfColumns,
  columnNames,
  columnTypes) {
  let Route = "Parse"

  return new Promise((res, rej) => {
    var options = {
      'method': 'POST',
      'url': `${H2O_HOST}${H2O_API_PREFIX}${Route}`,
      'headers': {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      form: {
        'number_columns': `${nrOfColumns}`,
        'source_frames': `[ "${sourceFrame}" ]`,
        'destination_frame': `${destinationFrame}`,
        'column_names': `[${columnNames}]`,
        'column_types': `[${columnTypes}]`,
        'parse_type': 'CSV',
        'separator': '124',
        'single_quotes': false,
        'check_header': 1,
        'delete_on_done': true,
        'chunk_size': '4194304'
      }

    };

    request(options, (error, response) => {
      if (error) {
        rej(error);
        return (error);
      }
      if (response.statusCode == 404) { res("Not found"); }
      res(response.body);
    });

  })
}

/**
 * Split a frame on Train and Test data, by a given percentage
 * @param {String} frame 
 * @param {float} percentage 
 * @param {double} seedValue random seed for random seed used in sampling
 * @returns 
 */
async function splitFrames(frame, percentage, seedValue) {

  return new Promise((res, rej) => {

    if (!seedValue) seedValue = 666999;

    var Route = "/99/Rapids?ast=";
    var frame = "PurchaseInvoiceLines"
    var percentage = 0.75

    var command =
      `(, (tmp= tmp_data (h2o.runif ${frame} ${seedValue})) (assign ${frame}_Train (rows ${frame} \
(<= tmp_data ${percentage}))) (assign ${frame}_Test (rows ${frame} \
(> tmp_data ${percentage}))) (rm tmp_data))`

    var options = {
      'method': 'POST',
      'url': `${H2O_HOST}${Route}${command}`
    };

    request(options, (error, response) => {
      if (error) {
        rej(error);
        return (error);
      }
      if (response.statusCode == 404) { res("Not found"); }
      res(response.body);
    });

  })

}

/**
 * 
 * Build a Gradient Boosting Machine model, using a training and a test dara frame 
 * @param {*} trainDF 
 * @param {*} testDF 
 * @param {*} ignoredCol 
 * @returns ModelID or 'Error'
 */

async function buildGBM(modelName, trainDF, testDF, ignoredCol) {

  let Route = "/3/ModelBuilders/gbm?";

  let command = `model_id=${modelName}&training_frame=${trainDF}&validation_frame=${testDF}&nfolds=0&response_column=TaxPercentage&ignored_columns=%5B%22${ignoredCol}%22%5D&ignore_const_cols=true&ntrees=50&max_depth=5&min_rows=10&nbins=20&seed=-1&learn_rate=0.1&sample_rate=1&col_sample_rate=1&score_each_iteration=false&score_tree_interval=0&balance_classes=false&max_confusion_matrix_size=20&nbins_top_level=1024&nbins_cats=1024&r2_stopping=1.7976931348623157e%2B308&stopping_rounds=0&stopping_metric=AUTO&stopping_tolerance=0.001&max_runtime_secs=0&learn_rate_annealing=1&distribution=AUTO&huber_alpha=0.9&col_sample_rate_per_tree=1&min_split_improvement=0.00001&histogram_type=AUTO&categorical_encoding=AUTO&monotone_constraints= &gainslift_bins=-1&auc_type=AUTO&build_tree_one_node=false&sample_rate_per_class=&col_sample_rate_change_per_level=1&max_abs_leafnode_pred=1.7976931348623157e%2B308&pred_noise_bandwidth=0&calibrate_model=false&check_constant_response=true`
  return new Promise((res, rej) => {
    var options = {
      'method': 'POST',
      'url': `${H2O_HOST}${Route}${command}`,
      'headers': {
      }
    };

    console.log("Build GBM", command);

    request(options, (error, response) => {
      if (error) {
        rej(error);
        return (error);
      }
      if (response.statusCode == 404) { res("Error"); }
      let r = JSON.parse(response.body);
      res(r.job.key.name);
    });

  })
}

/**
 * Build a DRF model, using a training and a test dara frame 
 * @returns 
 */
async function buildDRF(trainDF, testDF) {

  let Route = "/99/Rapids?ast=";
  if (!seedValue) seedValue = 666999;

  let command =
    `(, (tmp= tmp_data (h2o.runif ${frame}.hex ${seedValue}))\ (assign ${frame}_Train (rows ${frame}.hex \
          (<= tmp_data ${percentage}))) \
        (assign ${frame}_Test \
          (rows ${frame}.hex \
          (> tmp_data ${percentage}))) \
        (rm tmp_data)
     )`

  return new Promise((res, rej) => {
    var options = {
      'method': 'POST',
      'url': `${H2O_HOST}${Route}${command}`,
    };

    request(options, (error, response) => {
      if (error) {
        rej(error);
        return (error);
      }
      if (response.statusCode == 404) { res("Not found"); }
      res(response.body);
    });

  })

}

/**
 * Save a MOJO model into a file
 * @param {String} modelID Model to download as mojo
 * @param {String} filename Full path to the exported filename
 * @returns 
 */
async function saveMojo(internalID, modelID, filename) {

  let Route = "Models";

  const fs = require('fs');

  const file = fs.createWriteStream(filename);

  return new Promise((res, rej) => {
    var options = {
      'method': 'GET',
      'url': `${H2O_HOST}${H2O_API_PREFIX}${Route}/${modelID}/mojo`,
    };

    request(options, (error, response) => {
      if (error) {
        rej(error);
        return (error);
      }
      if (response.statusCode == 404) { res("Not found"); }
    }).pipe(file).on('close', () => { console.log("File downloaded") });
  })
}

module.exports = {
  hurricaneClassification: hurricaneClassification,
  importSourceData: importSourceDataH2O,
  parseFrame: parseFrame,
  getAllFrames: getAllFrames,
  getFrame: getFrame,
  checkFrame: checkFrame,
  checkJob: checkJob,
  buildDRF: buildDRF,
  buildGBM: buildGBM,
  saveMojo: saveMojo,
  splitFrames: splitFrames
}
