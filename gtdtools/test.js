var request = require('request');
const ML = require("./ml-tools.js");
const H2O_HOST = "http://127.0.0.1:54321"
const H2O_API_PREFIX = "/3/"
var seedValue = 666999;

var Route = "/99/Rapids?ast=";
var frame = "PurchaseInvoiceLines"
var percentage = 0.75

// const fn2 = new Promise((res, rej) => {
//   res(ML.splitDRFFrames("PurchaseInvoiceLines", 0.75, 817267));
// })

// async function checkFrame ( frameID, numRetries){

//   let r = await ML.getFrame(frameID);
//   let retry = 1;

//   console.log("R:[",r,"]");
//   console.log(typeof(r));

//   while ( (r === 'Not found') && (retry < numRetries) ){
//     r = await ML.getFrame(frameID);
//     retry++;
//     console.log(retry)
//   }
//   return ( r );
// }

const FILE = "/Users/valterpinho/dev/data-samples/PurchaseLines_all.csv";
const SOURCE_FRAME = "nfs://Users/valterpinho/dev/data-samples/PurchaseLines_all.csv";
const DESTINATION_FRAME = "PurchaseInvoiceLines";
const MODEL_NAME = "GBM_ModelOnPurchases";
const DOWNLOAD_PATH = "/Users/valterpinho/dev/gtd/mojo"

const fn = ML.buildGBM(MODEL_NAME, `${DESTINATION_FRAME}_Train`, `${DESTINATION_FRAME}_Test`, "TaxCode")

Promise.all([fn])
.then((ret) => {
  if ( ret[0] != 'Not found' ) {
    console.log("OK:",ret);
  } else {
    console.log("Super Erro");
  }
})