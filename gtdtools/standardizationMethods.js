const ML = require ( "./ml-tools")

/**
 *  
 * @param {object param 
 *    SKU: Stock keeping unit from the transaction 
 *    description: Item description      
 *    country: Country of origin 
 *    language: Language used for the description
 * @returns 
 */
async function runCodeStandardization(param) {

  const { SKU, description, country, language } = param;

  let classificationList = await ML.hurricaneClassification(SKU, description, country, language) 

  return (classificationList);
}

module.exports = { runCodeStandardization: runCodeStandardization}