
/**
 * Flatten an object 
 * e.g. { "A": 1, { "B":2, "C":3}} => { "A":1, "A.B":2, "A.C":3} 
 * @param {Object} ob 
 * @returns 
 */

function flattenObject(ob) {
  var toReturn = {};

  for (var i in ob) {
    if (!ob.hasOwnProperty(i)) continue;

    if ((typeof ob[i]) == 'object' && ob[i] !== null) {
      var flatObject = flattenObject(ob[i]);
      for (var x in flatObject) {
        if (!flatObject.hasOwnProperty(x)) continue;

        toReturn[i + '.' + x] = flatObject[x];
      }
    } else {
      toReturn[i] = ob[i];
    }
  }
  return toReturn;
}

function purgeReturn(retVar) {
  try {

    let ret = "empty";
    
    if (typeof (retVar) !== "undefined" || retVar !== null) {
      ret = retVar;
    }

    return (ret);
  }
  catch {
    return ("empty");
  }
}

function checkVarIntoJSON(variable) {
  if (typeof (variable) === 'undefined') {
    return ("{}");
  }
  return (variable);
}

module.exports = {
  flattenObject: flattenObject,
  checkVarIntoJSON: checkVarIntoJSON,
  purgeReturn:purgeReturn
}