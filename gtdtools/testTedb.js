const endpoint = 'https://ec.europa.eu/taxation_customs/tedb/ws/VatRetrievalService.wsdl'
const soap = require('soap');

function callTedb() {

  params = {
    retrieveVatRatesReqMsg: {
      memberStates: {
        isoCode: "AT"
      },
      situationOn: '2016-01-01',
      cnCodes: {
        value: "9619 00"
      }
    },
  }

  params2 = {
    memberStates: { isoCode: ["DE"] },
    categories: { identifier: ["REDUCED"] },
    from: "2018-01-01",
    to: "2022-01-01",
  };

  console.log("PARAMS for VIES:", params);
  soap.createClient(endpoint, (err, client) => {

    client.retrieveVatRatesAsync(params2, (err, result) => {
      console.log("RESULT:", result);
      console.log("ERR:", err);
    })
  })
}

callTedb();