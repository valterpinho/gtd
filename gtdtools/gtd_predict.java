import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import hex.genmodel.easy.RowData;
import hex.genmodel.easy.EasyPredictModelWrapper;
import hex.genmodel.easy.prediction.*;
import hex.genmodel.MojoModel;

/**
 * prediction <PATH to prediction_model> <PATH to prediction_frame>
 */
public class gtd_predict {
  public static void main(String[] args) throws Exception {

    String predictionModel = args[0] ;
    // String predictionModel = "/Users/valterpinho/Dev/gtd/mojo/GBM_PL.zip";
    String csvFile = args[1] ;
    // String csvFile = "/Users/valterpinho/dev/data-samples/PredictFrame.csv";


    EasyPredictModelWrapper model = new EasyPredictModelWrapper(
        new EasyPredictModelWrapper.Config()
            .setModel(MojoModel.load(predictionModel))
            .setConvertUnknownCategoricalLevelsToNa(true));

    hex.genmodel.GenModel mojo = MojoModel.load ( predictionModel );

    String[][] predictFrame = new String[100][9];
    int iLines = 0;

    // Index for prediction variable values
    int VAR_POSITION=6;

    try {

      File file = new File(csvFile);
      FileReader fr = new FileReader(file);
      BufferedReader br = new BufferedReader(fr);
      String line = "";
      String[] tempArr;

      while ((line = br.readLine()) != null) {

        tempArr = line.split("\\|");
        predictFrame[iLines] = tempArr;

        iLines++;
      }
      br.close();

    } catch (IOException ioe) {
      ioe.printStackTrace();
    }

    System.out.print("\n[");

    System.out.println( "\n [ { \"model\" : \"" + predictionModel + "\", ");
    System.out.println( "     \"predictionFrame\" : \"" + csvFile + "\" ");
    System.out.println( "     } ");
    System.out.println( " ], ");

    for (int i = 1; i < iLines; i++) {

      RowData row = new RowData();
      SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

      Date date;
      double mills;

      // SupplierTRN|SKU|ProductDescription|TaxDate|TaxExemptionCode|TaxType|TaxCountry|TaxCode|TaxPercentage
      row.put("SupplierTRN", predictFrame[i][0]);
      row.put("SKU", predictFrame[i][1]);
      row.put("ProductDescription", predictFrame[i][2]);
      date = (Date) formatter.parse(predictFrame[i][3]);
      mills = date.getTime();
      row.put("TaxDate", mills);
      row.put("TaxExemptionCode", predictFrame[i][4]);
      row.put("TaxType", predictFrame[i][5]);
      row.put("TaxCountry", predictFrame[i][6]);
      row.put("TaxPercentage", predictFrame[i][7]);
      // row.put("TaxCode", "ISE");
      // row.put("TaxPercentage", "0.0");

      MultinomialModelPrediction p = model.predictMultinomial(row);

      // System.out.println("\nCats : " + mojo.getDomainValues());
      String[][] domainValues = mojo.getDomainValues();

      System.out.println();
      // for (String[] x : domainValues) {
      //   if (x != null) {
      //     System.out.println(Arrays.asList(x));
      //   }
      // }

      System.out.print(" [\n  { \"sourceData\" : \"");
      System.out.print(predictFrame[i][0]);

      for (int k = 1; k < 7; k++) {
        System.out.print("|");
        System.out.print(predictFrame[i][k]);
      }
      System.out.println( "\" },");

      System.out.println("  { \"predictedTaxRate\": \"" + p.label + "\" },");
      System.out.println("  { \"classProbabilities\": [" );

      // System.out.println("  { \"***classProbabilities\": ["+Arrays.toString(p.classProbabilities) );
      // for (String[] x : domainValues) {
      //   if (x != null) {
      //     System.out.println("*** :DV"+Arrays.asList(x));
      //   }
      // }

      System.out.println("            [\"" + domainValues[VAR_POSITION][0] + "\" , " + p.classProbabilities[0] * 100 + "] ");

      for (int j = 1; j < p.classProbabilities.length; j++) {
        System.out.println("           ,[\"" + domainValues[VAR_POSITION][j] + "\" , " + p.classProbabilities[j]*100 + "] ");
      }
      System.out.println( "          ] ");
      System.out.println( "  }");
      System.out.print( " ]");
      if ( (i+1) < iLines) {
        System.out.println( " ,");
      }
    }
    System.out.println( "\n]");
  }
}
