const VIESEndpoint = 'http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl';
const soap = require('soap');

async function checkVies ( TRN, country ) {
  return new Promise ( (res,rej) => {
    let params = {
      countryCode: country,
      vatNumber: TRN
    };
    // params = { countryCode: 'PT', vatNumber: '510961304' }
    console.log ( "PARAMS for VIES:", params);
    soap.createClient(VIESEndpoint, (err, client) => {

      client.checkVat(params, (err, result) => {
        console.log("RESULT:",result);
        res(result);
      })

    })
})}

module.exports = {
  checkVies: checkVies
}