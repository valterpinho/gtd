const ML = require("./gtdtools/ml-tools.js");
const VIES = require("./gtdtools/vies.js");

class GTDData {
    constructor(ctx) {
        this.SKU = ctx["SKU"];
        this.Description = ctx["description"];
        this.language = ctx["language"];
        this.country = ctx["country"];
        this.eventCTX = ctx["event"]
        this.metadata = ctx["metadata"]
    }

    // Getters

    getSKU() {
        return this.clientSKU;
    }

    getDescription() {
        return this.Description;
    }

    getSupplierTRN() {
        let supTRN = this.metadata.SupplierTRN;
        if (!supTRN) {
            return ("None")
        }
        return (supTRN);
    }

    // Method for code standardization
    /**
     * 
     * @returns {object} List of suggested standardizations 
     */
    async codeStandardization() {
        const returnStandard = await ML.runCodeStandardization({ sourceCode: this.Description });

        return ({
            sourceCode: this.Description,
            standardCode: returnStandard
        })
    }

    // Method for checking VIES on tax reg number
    /**
     * 
     * @returns {object} List of suggested standardizations 
     */
    async checkVies() {

        let supTRN = this.getSupplierTRN();

        if (supTRN != "NONE") {
            return (VIES.checkVies(supTRN));

        }
        else
            return ("No VIES");
    }

    /**
     * taxDetermination 
     * @returns {float} Suggested tax determination 
     */
    taxDetermination() {
        return {
            sourceTax: this.eventCTX["tax"],
            sourceTaxDescription: this.Description,
            sourceTaxReason: this.eventCTX["taxReason"],
            suggestedTax: 0.23
        }
    }

}

let context = {
    SKU: '1982ABBA',
    description: 'Pencils',
    language: 'English',
    country: 'PT',
    metadata: { supplierTRN: '510961304' },
    event: {
        vatTax: '0', 
        sourceLocalization: 'Porto', 
        destinationLocalization: 'Lisboa' }
}


console.log("CTX:", context);

    const C = new GTDData(context);

    console.log("1");
    console.log("Descri:", C.getDescription());

    console.log("2");
    var X;

    C.codeStandardization().then((X) => {
        console.log("CodeStd:", X);
    })

console.log("CodeStd:", X);
    console.log("3");
    console.log("TaxD", C.taxDetermination());