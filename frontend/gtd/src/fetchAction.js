export const fetchAction = (body, action) => {

    const rootRoute = "http://localhost:4000";

    return (fetch(action, {
            method: 'POST',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(body)
        })
        .then((res) => res.json().catch(() => ({})))
        .then((res) => {
            const data = res || {}
            console.log("DATA: ", data);
        })
        .catch((e) => {
            throw new Error(e.message)
        }))
}