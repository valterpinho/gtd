import React, { useState } from 'react';
import './App.css';
import { fetchAction } from './fetchAction'

function App() {
  const [sourceItemCode, setSourceItemCode] = useState('')
  const [description, setDescription] = useState('')
  const [language, setLanguage] = useState('')
  const [country, setCountry] = useState('')
  const [metadata, setMetadata] = useState('')
  const [event, setEvent] = useState('')
  return (
    <div className="App">
      <header className="App-header">
        <h2>
          Tax Determination Using ML
        </h2>
      </header>
      <div className='ItemEntryForm'>
        <form>
          <div className='Item'>
            <label>
              Source Item Code
            </label>
            <input onChange={(e) => { setSourceItemCode(e.target.value) }} />
          </div>
          <div className='Item'>
            <label>
              Description
            </label>
            <input onChange={(e) => { setDescription(e.target.value) }} />
          </div>
          <div className='Item'>
            <label>
              Language
            </label>
            <input onChange={(e) => { setLanguage(e.target.value) }} />
          </div>
          <div className='Item'>
            <label>
              Country Code
            </label>
            <input onChange={(e) => { setCountry(e.target.value) }} />
          </div>
          <div className='Item'>
            <label>
              Metadata
            </label>
            <input className="ItemWide" onChange={(e) => { setMetadata(e.target.value) }} />
          </div>
          <div className='Item'>
            <label>
              Event Context
            </label>
            <input className="ItemWide" onChange={(e) => { setEvent(e.target.value) }} />
          </div>

          <div className="buttonContainer">

            <button className= "button-19" type='button' onClick={(e) => {
              console.log(sourceItemCode, description, language, country, metadata, event);
              console.log(e);
              fetchAction({ sourceItemCode, description, language, country, metadata, event }, "/vies");
            }}>VIES</button>

            <button className="button-19" type='button' onClick={(e) => {
              console.log(sourceItemCode, description, language, country, metadata, event);
              console.log(e);
              fetchAction({ sourceItemCode, description, language, country, metadata, event }, "/standardize");
            }}>STANDARDIZE</button>

            <button className="button-19" type='button' onClick={(e) => {
              console.log(sourceItemCode, description, language, country, metadata, event);
              console.log(e);
              fetchAction({ sourceItemCode, description, language, country, metadata, event }, "/determine");
            }}>DETERMINE</button>
          </div>

        </form>
      </div >
    </div >
  );
}

export default App;
